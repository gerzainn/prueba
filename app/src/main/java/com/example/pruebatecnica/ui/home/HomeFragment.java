package com.example.pruebatecnica.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pruebatecnica.FavoriteDatabase;
import com.example.pruebatecnica.Photo;
import com.example.pruebatecnica.PhotosAdapter;
import com.example.pruebatecnica.R;
import com.example.pruebatecnica.RecyclerViewScrollListener;
import com.example.pruebatecnica.UnsplashClient;
import com.example.pruebatecnica.UnsplashInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    PhotosAdapter adapter;
    PhotosAdapter.OnPhotoClickedListener photoClickListener;
    UnsplashInterface dataService;
    private int page = 1;

    public static FavoriteDatabase favoriteDatabase;

    RecyclerView recyclerView;
    ProgressBar progressBar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerView = root.findViewById(R.id.recyclerView);

        dataService = UnsplashClient.getUnsplashClient().create(UnsplashInterface.class);

        photoClickListener = new PhotosAdapter.OnPhotoClickedListener() {
            @Override
            public void photoClicked(Photo photo, ImageView imageView) {
                Intent intent = new Intent();
                intent.putExtra("image", photo);

            }

        };


        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new PhotosAdapter(new ArrayList<Photo>(), getActivity(), photoClickListener);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadphoto();
            }
        });


        loadphoto();




        return root;
    }


    private  void loadphoto()
    {
       dataService.getPhotos(page,null,"latest").enqueue(new Callback<List<Photo>>() {
           @Override
           public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {
               List<Photo> photos = response.body();
               Log.d("Photos", "Fotos " + photos.size());
               page++;
               adapter.addPhotos(photos);
               recyclerView.setAdapter(adapter);
           }

           @Override
           public void onFailure(Call<List<Photo>> call, Throwable t) {
            Log.e("Error", t.getMessage());
           }
       });
    }
}